<?php

namespace app\controllers;

use app\models\Category;
use app\models\CategorySearch;
use app\models\Photo;
use app\models\PhotoSearch;
use app\models\ProductCategory;
use Yii;
use app\models\Product;
use app\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionCategories($id) {
        $product = $this->findModel($id);
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->cSearch(Yii::$app->request->queryParams);
        $productCategories = Yii::$app->db->createCommand("SELECT category_id from product_category WHERE product_id=".$product->id)->queryColumn();
        return $this->render('categories', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'product'=>$product,
            'productCategories'=>$productCategories
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * assign category to product
     */
    public function actionAssign($id) {
        $product = $this->findModel($id);
        $cId = Yii::$app->request->get('category_id');
        if($cId) {
            $category = Category::findOne($cId);
            if($category) {
                $productCategory = ProductCategory::find()->where(['product_id'=>$product->id, 'category_id'=>$category->id])->one();
                if($productCategory) {
                    $productCategory->delete();
                    return $this->redirect(['categories', 'id' => $product->id]);
                } else {
                    $productCategory = new ProductCategory();
                    $productCategory->category_id = $category->id;
                    $productCategory->product_id = $product->id;
                    if($productCategory->save()) {
                        return $this->redirect(['categories', 'id' => $product->id]);
                    } else {
                        throw new NotFoundHttpException('The requested page does not exist.');
                    }
                }
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }


    public function actionPhotos($id) {
        $product = $this->findModel($id);
        $model = new Photo();
        $model->product_id = $id;

        if(Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->upload() && $model->save(false)) {
                $model = new Photo();
            }
        }

        $searchModel = new PhotoSearch();
        $dataProvider = $searchModel->pSearch(Yii::$app->request->queryParams, $id);
        return $this->render('photos', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'product'=>$product,
            'model'=>$model
        ]);
    }

    public function actionDeletePhoto($id) {
        $product = $this->findModel($id);
        $photo = Photo::findOne(Yii::$app->request->get('photo_id'));
        if($photo) {
            $photo->delete();
            return $this->redirect(['photos', 'id' => $product->id]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
