<?php

use yii\db\Migration;

class m170401_122111_initial extends Migration
{
    public function up()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `product` (
                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                      `name` varchar(255) NOT NULL,
                      `about` text,
                      `price` float NOT NULL DEFAULT '0',
                      `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
        );
        $this->execute("
                  CREATE TABLE IF NOT EXISTS `category` (
                  `id` bigint(20) NOT NULL AUTO_INCREMENT,
                  `parent_id` bigint(20) DEFAULT NULL,
                  `name` varchar(255) NOT NULL,
                  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
        $this->execute("
                CREATE TABLE IF NOT EXISTS `product_category` (
                  `category_id` bigint(20) NOT NULL,
                  `product_id` bigint(20) NOT NULL,
                  PRIMARY KEY (`category_id`,`product_id`),
                  KEY `category_id` (`category_id`),
                  KEY `product_id` (`product_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                ALTER TABLE `product_category`
                  ADD CONSTRAINT `product_key` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
                  ADD CONSTRAINT `category_key` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
        ");
        $this->execute("
            CREATE TABLE IF NOT EXISTS `discount` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `name` varchar(255) NOT NULL,
              `product_id` bigint(20) NOT NULL,
              `active_from` int(11) NOT NULL DEFAULT '0',
              `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `product_id` (`product_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
            ALTER TABLE `discount`
            ADD CONSTRAINT `discount_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `prodacts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
        ");
        $this->execute("
            CREATE TABLE IF NOT EXISTS `photo` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `parent_id` bigint(20) DEFAULT NULL,
              `product_id` bigint(20) NOT NULL,
              `file_path` varchar(255) NOT NULL,
              `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              `resolution` varchar(100) DEFAULT NULL,
              `status` int(1) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              KEY `product_id` (`product_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");


    }

    public function down()
    {
        $this->execute("DROP TABLE `category`, `discount`, `photo`, `prodacts`, `product_category`;");
        echo "m170401_122111_initial cannot be reverted.\n";
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
