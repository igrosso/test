<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PhotoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Photos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="photo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <ul class="nav nav-pills">
        <li>
            <a href="/product/update/<?=$product->id?>">Home</a>
        </li>
        <li class="active"><a href="/product/photos/<?=$product->id?>">Photos</a></li>
        <li><a href="/product/categories/<?=$product->id?>">Categories</a></li>
    </ul>
    <br />

    <div class="photo-form">

        <?php $form = \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

        <?= $form->field($model, 'imageFile')->fileInput() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php \yii\widgets\ActiveForm::end(); ?>

    </div>
    <br />
    <br />
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'file_path',
                'format' => 'raw',
                'content'=>function($data){
                    return Html::img($data->getPhotoUrl(300,300),[
                        'style' => 'width:300px;'
                    ]);
                }
            ],
            'date_create',
            // 'resolution',
            // 'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{delete_photo}',
                'buttons'=>[
                    'delete_photo' => function ($url, $model, $key) use ( $product) {
                        return Html::a('<button class="btn btn-danger">Remove</button>', ['delete-photo', 'id'=>$product->id, 'photo_id'=>$model->id]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
