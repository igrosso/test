<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <ul class="nav nav-pills">
        <li>
            <a href="/product/update/<?=$product->id?>">Home</a>
        </li>
        <li><a href="/product/photos/<?=$product->id?>">Photos</a></li>
        <li  class="active"><a href="/product/categories/<?=$product->id?>">Categories</a></li>
    </ul>
    <br />
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'parent_id',
            'name',
            'date_create',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{assign}',
                'buttons'=>[
                    'assign' => function ($url, $model, $key) use ($productCategories, $product) {
                        if($model->parent_id) {
                            if(in_array($model->id, $productCategories)) {
                                return Html::a('<button class="btn btn-danger">Remove</button>', ['assign', 'id'=>$product->id, 'category_id'=>$model->id]);
                            } else {
                                return Html::a('<button class="btn btn-success">Assign</button>', ['assign', 'id'=>$product->id, 'category_id'=>$model->id]);
                            }
                        } else {
                            return 'Parent Category';
                        }

                    }
                ]

            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
