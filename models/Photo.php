<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "photo".
 *
 * @property string $id
 * @property integer $parent_id
 * @property string $product_id
 * @property integer $file_path
 * @property integer $date_create
 * @property string $resolution
 * @property integer $status
 */
class Photo extends \yii\db\ActiveRecord
{
    public $imageFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['parent_id', 'product_id', 'status'], 'integer'],
            [['file_path'], 'string'],
            [['date_create'], 'safe'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg'],
            [['resolution'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'product_id' => Yii::t('app', 'Product'),
            'file_path' => Yii::t('app', 'File Path'),
            'date_create' => Yii::t('app', 'Date Create'),
            'resolution' => Yii::t('app', 'Resolution'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getPhotoUrl() {
        return '/'.$this->file_path;
    }

    public function upload()
    {
        $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
        if ($this->validate()) {
        
            $path = Yii::getAlias('@app/web/uploads/');
            if(!is_dir($path))
              mkdir($path,0777,true);
              
            $fileName = md5(microtime(true));           
            $this->imageFile->saveAs($path . $fileName . '.' . $this->imageFile->extension);
            $this->file_path = 'uploads/' . $fileName . '.' . $this->imageFile->extension;
            $this->imageFile  = null;
            return true;
        } else {
            return false;
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        $file = Yii::getAlias('@app/web').'/'.$this->file_path;
        if(file_exists($file)) {
            @unlink($file);
        }
    }
    /**
     * @inheritdoc
     * @return PhotoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PhotoQuery(get_called_class());
    }
}
