<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "discount".
 *
 * @property string $id
 * @property string $name
 * @property string $product_id
 * @property integer $active_from
 * @property string $date_create
 */
class Discount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'product_id'], 'required'],
            [['product_id'], 'validateProduct'],
            [['product_id', 'active_from'], 'integer'],
            [['date_create'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'product_id' => Yii::t('app', 'Product'),
            'active_from' => Yii::t('app', 'Active From'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }

    /**
     * @param $attr
     * @param $params
     * validate product
     */
    public function validateProduct($attr, $params)
    {
        if ($this->product_id) {
            $model = Product::findOne($this->product_id);
            if(!$model)
            $this->addError($attr, 'Product is incorrect.');
        }
    }
    /**
     * @inheritdoc
     * @return DiscountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DiscountQuery(get_called_class());
    }
}
