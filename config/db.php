<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=just_coded',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
